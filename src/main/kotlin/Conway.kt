import model.Game
import util.Rule
import java.lang.Math

class Conway {
    companion object {
        fun gameOfLive(rule: Rule, currentGeneration: Game): Game {
            val lines = currentGeneration.field.size
            val columns = currentGeneration.field[0].size

            val nextGeneration = Game(lines, columns)
            for (y in 0 until lines) {
                for (x in 0 until columns) {
                    if (currentGeneration.field[y][x]) {
                        nextGeneration.set(y, x, applyRules(rule, currentGeneration, y, x), currentGeneration.age[y][x])
                    } else {
                        nextGeneration.set(y, x, applyRules(rule, currentGeneration, y, x), 0)
                    }
                }
            }
            return nextGeneration
        }

        private fun applyRules(rule: Rule, game: Game, y: Int, x: Int): Boolean {
            val neighbours = countNeighbours(game.field, y, x)
            return if (game.field[y][x]) {
                game.age[y][x] = if (rule.survive.contains(neighbours)) 1.let {
                    game.age[y][x] += it; game.age[y][x]
                } else 0
                rule.survive.contains(neighbours)
            } else {
                rule.born.contains(neighbours)
            }
        }

        private fun countNeighbours(field: Array<BooleanArray>, y: Int, x: Int): Int {
            val lines = field.size
            val columns = field[0].size

            val yTop = getNeighbourPosition(y, 1, lines)
            val yBot = getNeighbourPosition(y, -1, lines)
            val xRight = getNeighbourPosition(x, 1, columns)
            val xLeft = getNeighbourPosition(x, -1, columns)

            // neighbours
            return listOf(
                Pair(yBot, xLeft), Pair(yBot, x), Pair(yBot, xRight),
                Pair(y, xLeft)                  , Pair(y, xRight),
                Pair(yTop, xLeft), Pair(yTop, x), Pair(yTop, xRight)
            )
                .map { pos -> field[pos.first][pos.second] }
                .filter { i -> i }
                .count()
        }

        private fun getNeighbourPosition(pos: Int, delta: Int, limit: Int): Int {
            return Math.floorMod((pos + delta), limit)
        }
    }
}