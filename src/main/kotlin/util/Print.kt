package util

import model.Cell
import model.Game

class Print private constructor() {
    companion object {
        fun field(game: Game, iteration: Int) {
            val column = StringBuilder()
            val columns = StringBuilder()
            columns.append("\r")
            for (y in game.field.indices) {
                for (x in game.field[y].indices) {
                    if (game.field[y][x]) {
                        column.append(colourMap(game.age[y][x])).append(Cell.ALIVE.value).append("\u001B[0m")
                    } else {
                        column.append(Cell.DEAD.value)
                    }
                }
                columns.append(column.toString()).append("\n")
                column.deleteRange(0, column.length)
            }

            columns.append("_".repeat(game.field[0].size))
            columns.append("Iteration: ").append(iteration)
            columns.append(" Length: ").append(game.field.size)
            columns.append(" Width: ").append(game.field[0].size)
            print(columns.toString())
        }

        private fun colourMap(age: Int): String {
            return when (age) {
                0 -> "\u001B[38;5;34m"
                1 -> "\u001B[38;5;35m"
                2 -> "\u001B[38;5;36m"
                3 -> "\u001B[38;5;37m"
                4 -> "\u001B[38;5;38m"
                5 -> "\u001B[38;5;39m"
                else -> "\u001B[38;5;21m"
            }
        }
    }
}