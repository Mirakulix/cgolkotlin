package util

import model.Game
import java.nio.file.Files
import java.nio.file.Paths


class Parser private constructor() {
    companion object {
        private const val CELLS_SUFFIX = ".cells"
        private const val RLE_SUFFIX = ".rle"
        private const val DEAD_CELL = '.'
        private const val ALIVE_CELL = '0'
        private const val DEAD_RLE = 'b'
        private const val ALIVE_RLE = 'o'

        fun parse(game: Game, filePath: String) {
            val file: List<String> = Files.readAllLines(Paths.get(filePath))

            val fileContent = getFileContent(file, filePath)
            if (fileContent.isNotEmpty()) {
                if (filePath.endsWith(CELLS_SUFFIX)) {
                    parseCells(game, fileContent)
                }
                if (filePath.endsWith(RLE_SUFFIX)) {
                    parseRle(game, fileContent)
                }
            }
        }

        private fun getFileContent(file: List<String>, filepath: String): List<String> {
            return when {
                filepath.endsWith(CELLS_SUFFIX) -> file.filter { f -> !f.startsWith("!") }
                filepath.endsWith(RLE_SUFFIX) -> {
                    file.filter { f -> !f.startsWith("#") }
                        .filter { f -> !f.startsWith("x") }
                        .filter { f -> !f.startsWith("y") }
                        .first { f -> !f.startsWith("rule") }
                        .split("\\$")
                }
                else -> listOf()
            }
        }

        private fun parseCells(game: Game, cellsFile: List<String>) {
            var x = 0
            var y = 0
            cellsFile.forEach { line ->
                for (i in 0..line.length) {
                    game.set(y, x, line[i] != DEAD_CELL, 0)
                    x++
                }
                x = 0
                y++
            }
        }

        private fun parseRle(game: Game, rleFile: List<String>) {
            val cellsFile = rleToCells(rleFile)

            parseCells(game, cellsFile)
        }

        private fun rleToCells(rleFile: List<String>): List<String> {
            val repeater = StringBuilder()
            var cellsFile: List<String> = mutableListOf()
            val line = StringBuilder()

            rleFile.forEach { rleLine ->
                for (i in 0..rleLine.length) {
                    when (rleLine[i]) {
                        DEAD_RLE -> appendToLine(repeater, line, DEAD_CELL)
                        ALIVE_RLE -> appendToLine(repeater, line, ALIVE_CELL)
                        else -> repeater.append(rleLine[i])
                    }
                }
                cellsFile = cellsFile + line.toString()
                line.replaceRange(0, line.length, "")
            }
            return cellsFile
        }

        private fun appendToLine(repeater: StringBuilder, line: StringBuilder, aliveCell: Char) {
            if (repeater.toString().isNotBlank()) {
                line.append(aliveCell)
            } else {

                line.append(aliveCell.toString().repeat(repeater.toString().toInt()))
                repeater.replaceRange(0, repeater.length, "")
            }
        }
    }
}

