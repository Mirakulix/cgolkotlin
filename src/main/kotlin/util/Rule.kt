package util

import java.lang.Character.getNumericValue

class Rule(private var rule: String) {
    val survive = mutableListOf<Int>()
    val born = mutableListOf<Int>()

    init {
        rule = rule.toLowerCase()
        rule = rule.trim()
        // validation
        if (rule.contains("/")) {
            val x = rule.split("/")
            if (x[0].startsWith("b") && x[1].startsWith("s")) {
                x[0].toCharArray()
                    .drop(1)
                    .forEach { born.add(getNumericValue(it)) }
                x[1].toCharArray()
                    .slice(1..x.size)
                    .forEach { survive.add(getNumericValue(it)) }
            } else {
                print("Wrong format, pls type \"b/s numbers / s/b numbers\"")
            }
        }
    }
}