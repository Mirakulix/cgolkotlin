import model.Game
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import util.Parser.Companion.parse
import util.Print
import util.Rule


@Command(name = "Conways game of life", mixinStandardHelpOptions = true, version = ["1.0.0"])
class Application : Runnable {

    @Option(names = ["-c"], description = ["Columns (x axis)"], arity = "1", defaultValue = "318")
    private var columns = 0

    @Option(names = ["-r"], description = ["Rows (y axis)"], arity = "1", defaultValue = "73")
    private var lines = 0

    @Option(names = ["-i"], description = ["Amount of conway iterations"], arity = "1", defaultValue = "1000")
    private var iterations = 0

    @Option(
        names = ["-s"],
        description = ["Sleep (time between two renderings)"],
        arity = "1",
        defaultValue = "16"
    ) // ~ 60 hz
    private var sleep = 0L

    @Option(names = ["-p"], description = ["Start conway with a specific pattern"], arity = "1", defaultValue = "")
    private var patternPath: String = ""

    @Option(
        names = ["--rule"],
        description = ["Cellular automaton rule, e.g. B36/S23 for highlife."],
        arity = "1",
        defaultValue = "B3/S23"
    )
    private var rule: String = ""

    override fun run() {
        println("\u001B[2J")
        var game = Game(lines, columns)
        if (patternPath.isBlank()) {
            game.init()
        } else {
            parse(game, patternPath)
        }
        val currentRule = Rule(rule)
        for (i in 0 until iterations) {
            Print.field(game, i)
            Thread.sleep(sleep)
            game = Conway.gameOfLive(currentRule, game)
        }
    }
}

fun main(args: Array<String>) {
    CommandLine(Application()).execute(*args)
}