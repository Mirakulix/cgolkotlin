package model

import java.security.SecureRandom


data class Game(val lines: Int, val columns: Int) {
    val field = Array(lines) { BooleanArray(columns) }
    val age = Array(lines) { IntArray(columns) }

    fun init() {
        val random = SecureRandom.getInstanceStrong()
        for (element in field) {
            for (x in field[0].indices) {
                element[x] = random.nextBoolean()
            }
        }
    }

    fun set(y: Int, x: Int, value: Boolean, ageValue: Int) {
        field[y][x] = value
        age[y][x] = ageValue
    }
}
