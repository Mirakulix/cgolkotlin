FROM oracle/graalvm-ce:20.2.0-java11 AS builder

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV JAVA_VERSION="20.2.0.r11-grl"
ENV GRADLE_VERSION="6.6.1"

WORKDIR /$HOME

RUN yum install -y curl zip unzip
RUN curl -s "https://get.sdkman.io" | bash

RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh && \
        yes | sdk install gradle $GRADLE_VERSION && \
        rm -rf $HOME/.sdkman/archives/* && \
        rm -rf $HOME/.sdkman/tmp/*"

RUN gu install native-image

CMD java -version