# Conway's Game of Life in Java

## Install instructions
Download the latest release from cgol  https://gitlab.com/Mirakulix/cgoljava/-/releases
```bash
cd ~/Downloads
chmod +x cgol
```

## Run cgol
```bash
./cgol --help 
Usage: Conways game of life [-hV] [-c=<columns>] [-i=<iterations>]
                            [-p=<patternPath>] [-r=<lines>] [-s=<sleep>]
  -c=<columns>        Columns (x axis)
  -h, --help          Show this help message and exit.
  -i=<iterations>     Amount of conway iterations
  -p=<patternPath>    Start conway with a specific pattern
  -r=<lines>          Rows (y axis)
  -s=<sleep>          Sleep (time between two renderings)
  -V, --version       Print version information and exit.
```

## Build it yourself
#### Prerequisite
graalvm 20.2.0 with java version 11 e.g. from https://sdkman.io/

```bash
./build.sh
```
